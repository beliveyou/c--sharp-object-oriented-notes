#### C # sharp

**输出**： Console. Write('''')

​			Console. Writeline()自动换行

**输入**： Console.ReadLine();

**数据类型**：整形，浮点，字符，布尔(bool),string,decimal(可以和整形运算，不能与浮点运算)

decimal a = 数字+M

**三元** ： （条件表达式--> true/false）? true : false

例如：int  max = a>b? a:b;

比较三个数的大小 int max = a>b?(a>c?a:c):(b>c?b:c);

**循环四要素**：初始值，判断条件，循环体，自增

**while** ：知道推出条件时，不知道循环次数

**foreach**(一个一个子的自动换行)

**var** : 隐式声明变量

