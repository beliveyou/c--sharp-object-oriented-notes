### 转义符

#### \n 表示换行

换行仅仅是换行，不表示完成，也不把[光标](https://so.csdn.net/so/search?q=光标&spm=1001.2101.3001.7020)放到行首

#### \s 表示空格

什么都不输入

#### \r 表示回车

回车仅仅是表示完成，把光标回到行首

PC的回车键就是先换行 **\n** 再回车 **\r**

#### \t 表示制表符

表示制表符,相当于键盘上的Tab键按一次的效果

### Convert 类型转换

|         方法         |            说明            |
| :------------------: | :------------------------: |
|  Convert.ToInt16()   |     转换为整型(short)      |
|  Convert.ToInt32()   |      转换为整型(int)       |
|  Convert.ToInt64()   |      转换为整型(long)      |
|   Convert.ToChar()   |     转换为字符型(char)     |
|  Convert.ToString()  |   转换为字符串型(string)   |
| Convert.ToDateTime() |   转换为日期型(datetime)   |
|  Convert.ToDouble()  | 转换为双精度浮点型(double) |
|  Conert.ToSingle()   | 转换为单精度浮点型(float)  |

```c#
class Program
{
    static void Main(string[] args)
    {
         float num1 = 82.26f;
         int integer;
         string str;
         integer = Convert.ToInt32(num1);
         str = Convert.ToString(num1);
         Console.WriteLine("转换为整型数据的值{0}", integer);
         Console.WriteLine("转换为字符串{0},",str);
    }
}
```

### 异常捕获(try)

**try**：一个 try 块标识了一个将被激活的特定的异常的代码块。后跟一个或多个 catch 块。

**catch**：程序通过异常处理程序捕获异常。catch 关键字表示异常的捕获。

**finally**：finally 块用于执行给定的语句，不管异常是否被抛出都会执行。

###  **const** 常量

常量是固定值，程序执行期间不会改变。常量可以是任何基本数据类型

常量是使用 **const** 关键字来定义的 。

```c#
const <data_type> <constant_name> = value;
```

### 枚举（Enum）

声明枚举的一般语法：

```c#
enum <enum_name>
{ 
    enumeration list 
};
```

*enum_name* 指定枚举的类型名称。

*enumeration list* 是一个用逗号分隔的标识符列表。

枚举列表中的每个符号代表一个整数值，一个比它前面的符号大的整数值。默认情况下，第一个枚举符号的值是 0.例如：

```C#
enum Days { Sun, Mon, tue, Wed, thu, Fri, Sat };
```

### 结构体（Struct）

**定义结构体**

```C#
struct Books
{
   public string title;
   public string author;
   public string subject;
   public int book_id;
};  
```

### out 参数修饰符

**out关键字通过引用传递参数。 它让形参成为实参的别名，这必须是变量**

```C#
void Method(out int answer, out string message, out string stillNull) 
{    
    answer = 44;    
    message = "I've been returned";    
    stillNull = null; 
} 
	int argNumber; 
	string argMessage, argDefault; 
	Method(out argNumber, out argMessage, out argDefault); 
	Console.WriteLine(argNumber); 
	Console.WriteLine(argMessage); 
	Console.WriteLine(argDefault == null);
```

### ref 参数修饰符

**变量作为参数传给方法，同时希望在方法执行完成后，对参数所做的修改能够反映到变量上**

```C#
static void TestRef(int x, int y)
{
    int temp;
    temp = y;
    y = x;
    x = temp;
}
static void Main(string[] args)
{
    int x = 10;
    int y = 20;
    Console.WriteLine("交换前：x={0},y={1}", x, y);
    TestRef(x, y);
    Console.WriteLine("交换后：x={0},y={1}", x, y);
    Console.ReadKey();
}
```

### this关键字

#### 1.this代表当前类的实例对象

```C#
public class Test
{
    public string Name{get;set;}      
    public void TestChange(string strName)
    {
        this.Name=strName;
    }
}
```

#### 2.构造函数重载

```C#
public class Test
{
    public Test()
    {
        Console.WriteLine("无参构造函数");
    }
    // this()对应无参构造方法Test()
　 // 先执行Test()，后执行Test(string text)
    public Test(string text) : this()
    {
        Console.WriteLine(text);
        Console.WriteLine("有参构造函数");
    }
}
```

### params

**在声明方法时参数类型或者个数不确定时使用**

**params参数必须是参数表的最后一个参数**

```C#
namespace ConsoleApp1
{
    class Program
    {
        static void Main()
        {
            UseParams(1, 2, 3); //既可以用任意多个int        
int[] myarray = new int[3] { 10, 11, 12 };
        UseParams(myarray); //也可以是int一维数组  
 
        UseParams2(1, 'a', new object() );
    }
 
    public static void UseParams(params int[] list)
    {
        for (int i = 0; i < list.Length; i++)
        {
            Console.WriteLine(list[i]);
        }
        Console.WriteLine();
    }
 
    public static void UseParams2(params object[] list)
    {
        for (int i = 0; i < list.Length; i++)
        {
            Console.WriteLine(list[i]);
        }
        Console.WriteLine();
    }
}
```
### 字符串方法

**Length	获取字符串的长度，即字符串中字符的个数**

```C#
class Program

{

    static void Main(string[] args)

    {

        string str = Console.ReadLine();

        Console.WriteLine("字符串的长度为：" + str.Length);

        Console.WriteLine("字符串中第一个字符为：" + str[0]);

        Console.WriteLine("字符串中最后一个字符为：" + str[str.Length - 1]);

    }

}
```

**IndexOf	返回整数，得到指定的字符串在原字符串中第一次出现的位置**

```C#
class Program

{

    static void Main(string[] args)

    {

        string str = Console.ReadLine();

        if (str.IndexOf("@") != -1)

        {

            Console.WriteLine("字符串中含有@，其出现的位置是{0}", str.IndexOf("@") + 1);

        }

        else

        {

            Console.WriteLine("字符串中不含有@");

        }

    }

}
```

**LastlndexOf	返回整数，得到指定的字符串在原字符串中最后一次出现的位置**

```C#
class Program
{
    static void Main(string[] args)
    {
        string str = Console.ReadLine();
        int firstIndex = str.IndexOf("@");
        int lastIndex = str.LastIndexOf("@");
        if(firstIndex != -1)
        {
            if (firstIndex == lastIndex)
            {
                Console.WriteLine("在该字符串中仅含有一个@");
            }
            else
            {
                Console.WriteLine("在该字符串中含有多个@");
            }
        }
        else
        {
            Console.WriteLine("在该字符串中不含有@");
        }
    }
}
```

**StartsWith	返回布尔型的值，判断某个字符串是否以指定的字符串开头**

```C#
using System;
public class Demo {
   public static void Main() {
      string str = "JohnAndJacob";
      Console.WriteLine("String = "+str);
      Console.WriteLine("字符串是否以 J 开头? = "+str.StartsWith("J"));
      char[] destArr = new char[20];
      str.CopyTo(1, destArr, 0, 4);
      Console.Write(destArr);
   }
}
**15	Replace	返回一个新的字符串，用于将指定字符串替换给原字符串中指定的字符串**
```

```C#
class Program
{
    static void Main(string[] args)
    {
        string str = Console.ReadLine();
        if (str.IndexOf(",") != -1)
        {
            str = str.Replace(",", "_");
        }
        Console.WriteLine("替换后的字符串为：" + str);
    }
}
```

**Substring	返回一个新的字符串，用于截取指定的字符串**

```C#
class Program
{
    static void Main(string[] args)
    {
        string str = Console.ReadLine();
        int firstIndex = str.IndexOf("@");
        int lastIndex = str.LastIndexOf("@");
        if(firstIndex != -1 && firstIndex == lastIndex)
        {
            str = str.Substring(0, firstIndex);
        }
        Console.WriteLine("邮箱中的用户名是：" + str);
    }
```

**Insert	返回一个新的字符串，将一个字符串插入到另一个字符串中指定索引的位置**

```C#
class Program
{
    static void Main(string[] args)
    {
        string str = Console.ReadLine();
        str = str.Insert(1, "@@@");
        Console.WriteLine("新字符串为：" + str);
    }
}
```

**Convert    是数据类型转换中最灵活的方法，它能够将任意数据类型的值转换成任意数据类型，前提是不要超出指定数据类型的范围。**

```C#
class Program
{
    static void Main(string[] args)
    {
         float num1 = 82.26f;
         int integer;
         string str;
         integer = Convert.ToInt32(num1);
         str = Convert.ToString(num1);
         Console.WriteLine("转换为整型数据的值{0}", integer);
         Console.WriteLine("转换为字符串{0},",str);
    }
}
```

**Parse	用于将字符串类型转换成任意类型**

```C#
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("请输入三个数字");
        int num1 = int.Parse(Console.ReadLine());
        int num2 = int.Parse(Console.ReadLine());
        int num3 = int.Parse(Console.ReadLine());
        int maxvalue = num1;
        if (num2 > maxvalue)
        {
            maxvalue = num2;
        }
        if (num3 > maxvalue)
        {
            maxvalue = num3;
        }
        Console.WriteLine("三个数中最大的值是：" + maxvalue);
    }
}
```

#### 数据类型转换

**隐式数据类型转换**：隐式转换主要是在整型、浮点型之间的转换，将存储范围小的数据类型直接转换成存储范围大的数据类

```C#
int a = 100;
double d = a;  //将int类型转换为double类型
float f = 3.14f;
d = f;    //将float类型转换为double类型
```

**强制数据类型转换**：强制类型转换主要用于将存储范围大的数据类型转换成存储范围小的，但数据类型需要兼容。

```C#
double dbl_num = 12345678910.456;
int k = (int) dbl_num ;//此处运用了强制转换
```

### 封装

**封装** 被定义为"把一个或多个项目封闭在一个物理的或者逻辑的包中"。在面向对象程序设计方法论中，封装是为了防止对实现细节的访问。

封装根据具体的需要，设置使用者的访问权限，并通过 **访问修饰符** 来实现。

一个 **访问修饰符** 定义了一个类成员的范围和可见性

**public**：所有对象都可以访问；

**private**：对象本身在对象内部可以访问；

**protected**：只有该类对象及其子类对象可以访问

**internal**：同一个程序集的对象可以访问；

**protected internal**：访问限于当前程序集或派生自包含类的类型。

### 构造函数（构造方法）

**构造方法 **的定义语法形式如下。

访问修饰符 类名 (参数列表)
{
  语句块；
}

这里构造方法的访问修饰符通常是**public**类型的，这样在其他类中都可以创建该类的对象。

```C#
class User

{
    public User(string name, string password, string tel
	{
        this.Name = name;
        this.Password = password;
        this.Tel = tel;

    }

    public string Name { get; set; }

    public string Password { get; set; }

    public string Tel { get; set; }


    public void PrintMsg()

    {

       Console.WriteLine("用户名：" + **this**.Name);

       Console.WriteLine("密  码：" + **this**.Password);

       Console.WriteLine("手机号：" + **this**.Tel);

    }

}
class Program
{
    static void Main(string[] args)
    {
        User user = new User("小明","123456","13131351111");
        user.PrintMsg();
    }
}
```

### 继承

**继承允许根据一个类来定义另一个类**

继承语法：

```C#
<访问修饰符> class<基类>
{
 ...
}
class <派生类> : <基类>
{
 ...
}
```

```C#
using** System;
**namespace** InheritanceApplication
{
   //基类
  **class** Shape
  {
   **public** **void** setWidth(**int** w)
   {
     width = w;
   }
   **public** **void** setHeight(**int** h)
   {
     height = h;
   }
   **protected** **int** width;
   **protected** **int** height;
  }

  *// 派生类*
  **class** Rectangle: Shape
  {
   **public** **int** getArea()
   {
     **return** (width * height);
   }
  }
  
  **class** RectangleTester
  {
   **static** **void** Main(**string**[] args)
   {
     Rectangle Rect = new Rectangle();

     Rect.setWidth(5);
     Rect.setHeight(7);

     *// 打印对象的面积*
     Console.WriteLine("总面积： {0}",  Rect.getArea());
     Console.ReadKey();
   }
  }
}
```

**一个类可以派生自多个类或接口，这意味着它可以从多个基类或接口继承数据和函数。**

派生类继承基类的属性和方法，但是不能使用基类的私有字段，

#### 继承的单根性：

**是指派生类只能继承一个基类**

#### 继承的传递性：

**派生类也可以作为其他类的基类。a继承b。a可以调用b的方法和属性，但是b又继承了c所以a也可以调用c的方法和属性。**

### base关键字

在继承中派生类并没有继承基类的构造函数，但是，派生类会默认调用基类无参的构造函数创建基类对象，让派生类可以使用基类中的对象。

所以，如果基类中写了一个有参的构造函数之后，无参被清除，派生类就无法调用对象，程序排错。

解决办法：

1、在基类中手写一个无参的构造函数。

2、在派生类中调用基类的构造函数使用关键字：**base()**



**object 是所有类的基类**

### new关键字

1、创建对象

2、隐藏从基类继承的重名的同名的成员

### 里氏转换

**里氏转换:派生类的对象可以赋值给基类 如果基类含有派生类的对象,那么基类可以强制转换成派生类**

```C#
//将派生类对象赋值给基类
Student s = new Student(); 
Person p = s; 
//同时可以简写成
Person p=new Student
```

因为基类含有派生类的成员,所以可以强制转换

```C#
Student s = new Student();
Person p = s;
//将基类强制转换成派生类
Student ss=(Student)p;
```

**基类必须含有派生类的对象才可以强制转换**

### 访问修饰符

**Public**：公有的，是类型和类型成员的访问修饰符。对其访问没有限制。

**Private**：私有的，是一个成员访问修饰符。只有在声明它们的类和结构中才可以访问。

**Protected**：受保护的，是一个成员访问修饰符。只能在它的类和它的派生类中访问。

**Internal**：内部的，是类型和类型成员的访问修饰符。同一个程序集中的所有类都可以访问。

**protected internal**：访问级别为 internal 或 protected。即，“同一个程序集中的所有类，以及所有程序集中的子类都可以访问。

### ArrayList 集合 和 (动态数组）

集合：很多数据的一个集合

**集合与数组的区别**

**集合**：长度可以任意改变，类型随便

**数组**：长度不可变，类型单一



```C#
using System;
using System.Collections;

namespace CollectionApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList al = new ArrayList();        
		Console.WriteLine("Adding some numbers:");
        al.Add(45);
        al.Add(78);
        al.Add(33);
        al.Add(56);
        al.Add(12);
        al.Add(23);
        al.Add(9);
       
        Console.WriteLine("Capacity: {0} ", al.Capacity);
        Console.WriteLine("Count: {0}", al.Count);
                 
        Console.Write("Content: ");
        foreach (int i in al)
        {
            Console.Write(i + " ");
        }
        Console.WriteLine();
        Console.Write("Sorted Content: ");
        al.Sort();
        foreach (int i in al)
        {
            Console.Write(i + " ");
        }
        Console.WriteLine();
        Console.ReadKey();
    }
   }
}
```
**将一个对象输出到控制台，默认情况下输出的是他的命名空间**

 **ArrayList** 类的一些常用的 **属性**：

| 属性           | 描述                                                  |
| :------------- | :---------------------------------------------------- |
| Capacity       | 获取或设置 ArrayList 可以包含的元素个数。             |
| Count          | 获取 ArrayList 中实际包含的元素个数。                 |
| IsFixedSize    | 获取一个值，表示 ArrayList 是否具有固定大小。         |
| IsReadOnly     | 获取一个值，表示 ArrayList 是否只读。                 |
| IsSynchronized | 获取一个值，表示访问 ArrayList 是否同步（线程安全）。 |
| Item[Int32]    | 获取或设置指定索引处的元素。                          |
| SyncRoot       | 获取一个对象用于同步访问 ArrayList。                  |

### 集合的方法

#### clear()

**list.Clear(**) 清空集合的所有元素

```C#
ArrayList al = new ArrayList();
                al.Add(45);
                al.Add(78);
                al.Add(33);
                al.Add(56);
                al.Add(12);
                al.Add(23);
                al.Add(9);
				//清空集合
                a1.Clear();
```

#### Remove()

**Remove()** 删除单个元素，写谁就删谁

```c#
ArrayList al = new ArrayList();        
        al.Add(45);
        al.Add(78);
        al.Add(33);
        al.Add(56);
        al.Add(12);
        al.Add(23);
        al.Add(9);
       al.Remove(45);
```

#### RemoveAt()

**RemoveAt()** 根据下标删除元素

```C#
ArrayList al = new ArrayList();
            al.Add(45);
            al.Add(78);
            al.Add(33);
            al.Add(56);
            al.Add(12);
            al.Add(23);
            al.Add(9);
            //al.Clear();
            //al.Remove(45);
            al.RemoveAt(0);
```

**RemoveRange()**

**RemoveRange()** 根据下标去删除一定范围的元素

```C#
ArrayList al = new ArrayList();
            al.Add(45);
            al.Add(78);
            al.Add(33);
            al.Add(56);
            al.Add(12);
            al.Add(23);
            al.Add(9);
            al.RemoveRange(1, 5);
```

#### Reverse()

**Reverse()** 反转集合

```C#
ArrayList al = new ArrayList();
            al.Add(45);
            al.Add(78);
            al.Add(33);
            al.Add(56);
            al.Add(12);
            al.Add(23);
            al.Add(9);
            al.Reverse();
```

#### Sort()

**Sort()** 升序排序集合

```C#
ArrayList al = new ArrayList();
            al.Add(45);
            al.Add(78);
            al.Add(33);
            al.Add(56);
            al.Add(12);
            al.Add(23);
            al.Add(9);
            al.Sort();
```

#### Insert()

**Insert()** 根据下标插入元素

```c#
ArrayList al = new ArrayList();
            al.Add(45);
            al.Add(78);
            al.Add(33);
            al.Add(56);
            al.Add(12);
            al.Add(23);
            al.Add(9);
            al.Insert(1,1);
```

**Contains()**

**Contains**  判断是否包含某个元素



```c#
         ArrayList p = new ArrayList();
            p.Add(true);
            p.Add("张三");
            p.Add(10);
        for (int i = 0; i < p.Count; i++)
        {
            Console.WriteLine(p[i]);
        }
        bool b = p.Contains(10);
        Console.WriteLine(b);
```

#### 集合的长度问题

每次集合中实际包含 的元素个(conut)超过了可以包含的元素的个数(capcity)的时候，集合就会向内存中省情多开辟一倍的空间，来保证集合的长度一直够用。

### HashTable 集合(键值对集合)

在键值对集合中，是根据键去找值的

键值对集合当中键必须是唯一的，值可以是重复的

```c#
Hashtable ht = new Hashtable();
            ht.Add(1, "张三");
            ht.Add(2, true);
            ht[3] = '男';
            ht.Add(false, "错误的");
            Console.WriteLine(ht[1]);
            Console.WriteLine(ht[2]);
            Console.WriteLine(ht[3]);
            Console.WriteLine(ht[false]);
```

#### ContainsKey()

**ContainsKey()** 判断键值对集合是否包含这个键

```C#
Hashtable ht = new Hashtable();
            ht.Add(1, "张三");
            ht.Add(2, true);
            ht [3] = '男';
            ht.Add(false, "错误的");
            ht.Add("zs", "张三");
            Console.WriteLine(ht[1]);
            Console.WriteLine(ht[2]);
            Console.WriteLine(ht[3]);
            Console.WriteLine(ht[false]);
            if (!ht.ContainsKey("zs"))
            {
                ht.Add("zs", "张三");
            }
            else
            {
                Console.WriteLine("已经包含这个键");
            }

  
```

### 泛式集合

可以声明类型的集合

```C#
List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);   
            list.Add(3);
            list.Add(4);
            foreach (int item in list)
            {
                Console.WriteLine(item);
            }
```

泛型集合可以和数组相互转换

```C#
List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);   
            list.Add(3);
            list.Add(4);
            foreach (int item in list)
            {
                Console.WriteLine(item);
            }
            //集合转数组
            int [] arr = list.ToArray();
            //数组转集合
            List<int>list2=arr.ToList(); 
```



### Path 类方法

#### File.Exist()

判断一个文件是否存在 返回bool值 

```C#
using System;
using System.Collections.Generic;
System.IO;
usingSystem.Linq;
using System.Text;
using System.Threading.Tasks;
namespace_File 操作类{
class Progran{
static void Main(string[] args){
if(File.Exists(@"C:\Users\Administrator\Desktop\file.txt"))
Console.WriteLine("文件存在！”)；
Console.Readkey();
                  }
         }
}
```

#### File.Create()

创建新的文件

```C#
  File.Create(@"C:\Users\yechengkai\Desktop\new.txt");
```

#### File.Delete()

删除文件

```C#
File.Delete(@"C:\Users\yechengkai\Desktop\new.txt");
```



#### GetFileName()

获得文件名

```C#
string str = @"H:\C#笔记\小新.mp4";
            Console.WriteLine(Path.GetFileName(str)); 
```

#### GetFileNameWithoutExtension()

从路径字符串中得到文件名(不带扩展名)

```C#
string Str1=@"C:\Users\Administrator\Desktop\测试文件.txt";

			Console.WriteLine("以下是不带扩展名的文件名：");

			Console.WriteLine(Path.GetFileNameWithoutExtension(Str1));
```

#### GetExtension()

从文件路径字符串中得到文件的扩展名

```C#
string Str1=@"C:\Users\Administrator\Desktop\测试文件.txt";

			Console.WriteLine("以下是文件扩展名：");

			Console.WriteLine(Path.GetExtension(Str1));
```

#### GetDirectoryName()

得到文件的文件夹路径

```C#
string Str1=@"C:\Users\Administrator\Desktop\测试文件.txt";

			Console.WriteLine("以下是文件所在文件夹路径。");

			Console.WriteLine(Path.GetDirectoryName(Str1));
```

#### GetFullPath()

从文件字符串中得到包括文件名和扩展名的全路径名。

```C#
string Str1=@"C:\Users\Administrator\Desktop\测试文件.txt";

			Console.WriteLine("以下是包括文件名和扩展名的全路径名");

			Console.WriteLine(Path.GetFullPath(Str1));
```

#### Combine()

合并两个文件路径字符串。

```C#
string Str1=@"C:\Users\Administrator\";

string Str2=@"Desktop\测试文件.txt";

			Console.WriteLine("下面是合并后的文件路径");

			Console.WriteLine(Path.Combine(Str1,Str2));
```

### File 类方法

#### ReadAllBytes()

将文件中的文字内容转成byte数组并返回。

```C# 
string Strpath = @"C:\Users\Administrator\Desktop\新建文件夹\新建文本文档.txt";  //赋值文本文件路径字符串

byte[] buffer = File.ReadAllBytes(Strpath);  //以二进制方式读取文本文件并返回byte数组

string StrContent = Encoding.Default.GetString(buffer); //以默认编码方式将二进制数组转换成string类型变量并返回
			Console.WriteLine(StrContent);
```

#### ReadAllLines()

以行的形式读取文本文件，并返回一个字符串数组

```C#
string Strpath = @"C:\Users\Administrator\Desktop\新建文件夹\新建文本文档.txt";  //赋值文本文件路径字符串
string[] Str = File.ReadAllLines(Strpath,Encoding.Default);   //以ReadAllLines方式读取，并填入编码参数

 foreach (string item in Str)  //输出每行内容
  {
     Console.WriteLine(item);
  }
```

#### ReadAllText()

读取文本，并返回字符串

```C#
string Strpath = @"C:\Users\Administrator\Desktop\新建文件夹\新建文本文档.txt";  //赋值文本文件路径字符串

string Str=File.ReadAllText(Strpath,Encoding.Default);

			Console.WriteLine(Str);
```

#### WriteAllBytes()

以二进制方式写入文件

```C#
string Strpath = @"C:\Users\Administrator\Desktop\新建文件夹\新建文本文档.txt";  //赋值文本文件路径字符串

string StrWrite = "大家好，我是XXXX"; //待写入的话

byte[] buffer = Encoding.Default.GetBytes(StrWrite);   //将要写入的话转成字节数组

File.WriteAllBytes(Strpath,buffer);  //以字节数组方式写入

			Console.WriteLine("写入成功！"); //写入成功提示
```

#### WiteAllLines()

以行方式写入文件

```C#
string Strpath = @"C:\Users\Administrator\Desktop\新建文件夹\新建文本文档.txt";  //赋值文本文件路径字符串

string[] Str={"大家早上好，这是我写的一句话"};

File.WriteAllLines(Strpath,Str);

Console.WriteLine("写入成功！"); //写入成功提示
```

#### WriteAllText()

以字符串方式写入文件

```C#
string Strpath = @"C:\Users\Administrator\Desktop\新建文件夹\新建文本文档.txt";  //赋值文本文件路径字符串

string Str = "大家好";

File.WriteAllText(Strpath,Str);

Console.WriteLine("写入成功！"); //写入成功提示
```

#### AppendAllText()

以追加方式写入文件，不会覆盖原有文件内容

```c#
string Strpath = @"C:\Users\Administrator\Desktop\新建文件夹\新建文本文档.txt";  //赋值文本文件路径字符串

File.AppendAllText(Strpath,"今天天气真好！",Encoding.Default);

Console.WriteLine("写入成功！");
```

### Dictionary 字典集合

**Dictionary字典集合和Hashtable集合相类似，都是键值对集合**

创建Dictionary集合：

```C#
Dictionary<键值类型,数据类型> 变量 = new Dictionary<键值类型,数据类型>();
Dictionary<int,string> dic = new Dictionary<int,string>();
```

访问键值对： 一对对遍历键值和数据

```C#
foreach(KeyValuePair<int,string> kv in dic){
	键值 = kv,key;
	数据 = kv.value;
```

### 装箱和拆箱

**装箱** 是将值类型转换为引用类型 

**拆箱** 是将引用类型转换为值类型

写代码过程中应该尽量避免装箱和拆箱

### FileStream 文件流

FileStream 操作字节，可以操作任何类型的文件

创建FileStream对象，参数：第一个是路径，第二个是文件模式FileMode，第三个数据模式FileAcess

```C#
FileStream devStream = new FileStream(devPath, FileMode.Append, FileAccess.Write, 				    										  FileShare.ReadWrite,512);
devStream.Write(data, 0, 128);
```

简单文件写入

```C# 
FileStream devStream = new FileStream(devPath, FileMode.Append, FileAccess.Write, 															  FileShare.ReadWrite,512);
	devStream.Write(data, 0, 128);
```

使用 **FileStream** 复制媒体文件

**using** 释放文件流的内存

```C#
using System;
using System.IO;
namespace _复制文件
{
    class Nq
    {
        public static void Main(string[] arge)
        //复制媒体文件
        {
            string duqu = @"C:\Users\yechengkai\Desktop\WeChat_20220227235931.mp4";
            string xieru = @"C:\Users\yechengkai\Desktop\new.mp4";
            CopyFile(duqu, xieru);
            Console.WriteLine("复制完成");
    }
    public static void CopyFile(string duqu, string xieru)
    {
        //创建一个负责读取的流
        using (FileStream b = new FileStream(duqu, FileMode.Open, FileAccess.Read))
        {
            byte[] n = new byte[1024 * 1024 * 5];
            //创建一个负责写入的流
            using (FileStream copyr = new FileStream(xieru, FileMode.OpenOrCreate, FileAccess.Write))
            {
                while (true)
                {
                    //读取本次读取的字节的长度
                    int r = b.Read(n, 0, n.Length);
                    //如果读取的字节长度为 0 就跳出这个循环
                    if (r == 0)
                    {
                        break;
                    }
                    copyr.Write(n, 0, r);
                }
            }
        }
    }
}
}
```
### 多态

#### 虚方法

当有一个定义在基类中的函数需要在继承类中实现时,可以使用虚方法,虚方法是使用关键字 **virtual** 在基类方法中声明，并在派生类中使用 **override** 声明。,虚方法可以在不同的继承类中有不同的实现,即为基类中定义的允许在派生类中重写的方法

```C#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _100
{
    public class Program
    {
        public static void Main(string[] arge)
        {
            N n = new N("小基");
            N2 n2 = new N2("小派");
            N[] str = { n, n2 };
            for (int i = 0; i < str.Length; i++)
            {
                str[i].Say();
            }
        }
    }
    class N
    {
    public string Name
    {
        get
        {
            return name;
        }

        set
        {
            name = value;
        }
    }
    public N(string Name)
    {
        this.name = Name;
    }
    public virtual void  Say()
    {
        Console.WriteLine("我是基类的方法{0}   ",Name);
    }
}
class N2 : N
{
    public N2(string name) : base(name)
    {

    }
    public override void Say()
    {
        Console.WriteLine("我是子类的方法{0}   ",this.Name);
    }
}
}
```

#### 抽象类

声明一个抽象方法使用 **abstract** 关键字。

一个抽象类可以包含抽象和非抽象方法，当一个类继承于抽象类，那么这个派生类必须实现所有的
的基类抽象方法。

一个抽象方法是一个没有方法体的方法。



```C#
using System;
namespace _抽象类
{
    public class 抽象类
    {
        public static void Main(string[] ager)
        {
            cx say = new name();
            say.cxl();
        }
    }
    //创建应该抽象类
    abstract class cx
    {
        //抽象类方法没有实现体
        public abstract void cxl();
    }
//创建一个类继承抽象类
class name : cx
{
    public override void cxl()
    {
        Console.WriteLine("我是重写的抽象类方法");
    }
  }
}
```
用多态模拟移动设备插到电脑上的读写

```C#
using System;
namespace _抽象类
{
    public class 抽象类
    {
        public static void Main(string[] ager)
        {
            Mobile ms = new MP3();
            Computer c = new Computer();
            c.Ms = ms;
            c.Read();
            c.Weite();
            ((MP3)ms).Music();
        }
    }
    //创建抽象父类已经抽象方法
    abstract class Mobile
    {
        public abstract void MobilRead();
        public abstract void MobilWrite();
}
//创建子类
class MobileDisk : Mobile
{
    public override void MobilRead()
    {
        Console.WriteLine("移动硬盘在读取");
    }
    public override void MobilWrite()
    {
        Console.WriteLine("移动硬盘在写入");
    }
}
class U : Mobile
{
    public override void MobilRead()
    {
        Console.WriteLine("U盘在读取");
    }
    public override void MobilWrite()
    {
        Console.WriteLine("U盘在写入");
    }
}
class MP3 : Mobile
{
    public override void MobilRead()
    {
        Console.WriteLine("MP3在读取");
    }
    public override void MobilWrite()
    {
        Console.WriteLine("MP3在写入");
    }
    public void Music()
    {
        Console.WriteLine("MP3在放音乐");
    }
}
class Computer
{
    private Mobile _ms;

    internal Mobile Ms
    {
        get
        {
            return _ms;
        }

        set
        {
            _ms = value;
        }
    }

    public void Read()
    {
        Ms.MobilRead();
    }
    public void Weite()
    {
        Ms.MobilWrite();
    }
  }
}
```

#### 接口

接口使用 **interface** 关键字声明，它与类的声明类似。接口声明默认是 public 的。

接口定义了所有类继承接口时应遵循的语法合同。接口定义了语法合同 **"是什么"** 部分，派生类定义了语法合同 **"怎么做"** 部分。

接口定义了属性、方法和事件，这些都是接口的成员。



```C#
using System;

interface IMyInterface
{
        // 接口成员
    void MethodToImplement();
}

class InterfaceImplementer : IMyInterface
{
    static void Main()
    {
        InterfaceImplementer iImp = new InterfaceImplementer();
        iImp.MethodToImplement();
    }
public void MethodToImplement()
{
    Console.WriteLine("MethodToImplement() called.");
  } 
}
```
